import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EntryPointComponent } from './app-user/entry-point-app/entry-point.component';



const routes: Routes = [
  { path: '', redirectTo:'marialex',pathMatch:'full'}
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes,{onSameUrlNavigation: 'reload'})
  ],
  exports: [RouterModule],
})
export class AppRoutingModule{ }
