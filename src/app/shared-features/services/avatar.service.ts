import { Injectable } from '@angular/core';
import { Urls } from '../constants/urls';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import { Avatar } from '../users/entities/avatar';


@Injectable()
export class AvatarService {
  private parentUrl:string = Urls.getBaseUrl();
  private headers = new Headers({'content-type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  
  constructor(private httpService:Http,private http: HttpClient) { }
  
  
  addAvatar(userId:number,files:File[]): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    for(let file of files){
      formData.append('imageFiles',file);
    }
    const req = new HttpRequest('POST', this.parentUrl+"users/user/"+userId+"/avartar/add", formData, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }

  getAvatarsByUserId(userId:number):Observable<Avatar[]>{
    return this.httpService
    .get(this.parentUrl+"users/user/"+userId+"/avartar/findAvatars",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  errorHnadler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }
  
}
