import { Injectable } from '@angular/core';

import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import { Urls } from '../constants/urls';
import { LoginCredentials } from '../users/entities/login-credentials';
import { AuthenticationDetails } from '../users/entities/authentication-details';
import { User } from '../users/entities/user';
import { Avatar } from '../users/entities/avatar';


@Injectable()
export class UserService {

  private parentUrl:string = Urls.getBaseUrl();
   private headers = new Headers({'content-type':'application/json'});
   private options = new RequestOptions({headers:this.headers});
   
   constructor(private httpService:Http) { }

  authenticateUser(loginCredentials:LoginCredentials): Observable<AuthenticationDetails>{
    return this.httpService
    .post(this.parentUrl+"authenticate",JSON.stringify(loginCredentials),this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHandler);
  }

  getUserDetails(token:string):Observable<User>{
    return this.httpService.post(this.parentUrl+"user/current-session-details",token,this.options)
    .map((res: any) => res.json())
    .flatMap((user: User) => {
      return this.httpService.get(this.parentUrl+"users/user/"+user.id+"/avartar/findAvatars",this.options)
        .map((res: any) => {
          let avatar:Avatar[]= res.json();
          user.avatar = avatar.reverse()[0];
          return user;
        });
    }).catch(this.errorHandler);
  }

  // getUserDetails(token:string):Observable<User>{
  //   return this.httpService
  //   .post(this.parentUrl+"user/current-session-details",token,this.options)
  //   .map((response:Response)=>response.json())
  //   .catch(this.errorHandler);
  // }
  
  registration(newUser:User):Observable<User>{
    return this.httpService
    .post(this.parentUrl+"user-register",JSON.stringify(newUser),this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHandler);
  }
  
  logout(){
    return this.httpService
    .get(this.parentUrl+"user/logout",this.options)
    .catch(this.errorHandler);
  }
  errorHandler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }
}
