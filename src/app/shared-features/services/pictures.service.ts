import { Injectable } from '@angular/core';
import { Urls } from '../constants/urls';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/forkJoin';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import { Avatar } from '../users/entities/avatar';
import { CoverPhoto } from '../users/entities/cover-photo';
import { Pictures } from '../models/pictures';

@Injectable()
export class PicturesService {
  
  private parentUrl:string = Urls.getBaseUrl();
  private headers = new Headers({'content-type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  
  constructor(private httpService:Http,private http: HttpClient) { }
  
  findPicturesByUserId(userId:number): Observable<Pictures>{
    return Observable.forkJoin([
      this.httpService.get(this.parentUrl+"users/user/"+userId+"/avartar/findAvatars")
      .map((response:Response)=>response.json())
      .toPromise(),
      this.httpService.get(this.parentUrl+"users/user/"+userId+"/cover-photo/findCoverPhotos")
      .map((response:Response)=>response.json())
      .toPromise()
    ]).map((data: any) => {
      let coverPhoto: CoverPhoto[] = data[1];
      let pictures:Pictures = new Pictures();
      pictures.avatars = data[0];
      pictures.coverPhoto = data[1];
      return pictures;
    })
  }
  
}
