import { TestBed, inject } from '@angular/core/testing';

import { CoverPhotoService } from './cover-photo.service';

describe('CoverPhotoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoverPhotoService]
    });
  });

  it('should be created', inject([CoverPhotoService], (service: CoverPhotoService) => {
    expect(service).toBeTruthy();
  }));
});
