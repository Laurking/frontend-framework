import { NgModule, ModuleWithProviders} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from '../components/footer/footer.component';
import { AuthGuard } from '../auth/auth.guard';
import { UnauthorizedAccessGuard } from '../auth/unauthorized-access.guard';
import { AuthService } from '../auth/auth.service';
import { UserService } from '../services/user-service';
import { VideoService } from '../../app-user/videos-app/services/video-service';
import { VideoSanitiserPipe } from '../pipes/videoSanitiser.pipe';
import { ProgressBarHandlerPipe } from '../pipes/progress-bar-handler.pipe';
import { FormatVideoTitlePipe } from '../pipes/format-video-title.pipe';
import { FormatFileSizePipe } from '../pipes/format-file-size.pipe';
import { ImageEmbedPipe } from '../pipes/image-embed.pipe';
import { PreviewVideoSanitizerPipe } from '../pipes/preview-video-sanitizer.pipe';
import { HighlightTextPipe } from '../pipes/highlight-text.pipe';
import { ReadMorePipe } from '../pipes/read-more.pipe';
import { PicturesService } from '../services/pictures.service';
import { CoverPhotoService } from '../services/cover-photo.service';
import { AvatarService } from '../services/avatar.service';
import { FindLastElementInArrayPipe } from '../pipes/find-last-element-in-array.pipe';










@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FooterComponent,
    VideoSanitiserPipe,
    ProgressBarHandlerPipe,
    FormatVideoTitlePipe,
    FormatFileSizePipe,
    ImageEmbedPipe,
    PreviewVideoSanitizerPipe,
    HighlightTextPipe,
    ReadMorePipe,
    FindLastElementInArrayPipe
  ],
  exports:[
    FooterComponent,
    VideoSanitiserPipe,
    ProgressBarHandlerPipe,
    FormatVideoTitlePipe,
    FormatFileSizePipe, 
    ImageEmbedPipe,
    PreviewVideoSanitizerPipe,
    HighlightTextPipe,
    ReadMorePipe,
    FindLastElementInArrayPipe
  ]
})
export class SharedModuleModule { 
    static forRoot(){
      return {
        NgModule:SharedModuleModule,
        providers:[AuthGuard, UnauthorizedAccessGuard,AuthService, UserService,VideoService,PicturesService, CoverPhotoService,AvatarService]
      };
    }
}
