import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UnauthorizedAccessGuard implements CanActivate {
  
  constructor(private router:Router){}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):boolean {
      if(localStorage.getItem('token') != null){
        this.router.navigate(['/marialex']);
        return false;
      }
      else{
        return true;
      }
      
    }
  }
  