import { TestBed, async, inject } from '@angular/core/testing';

import { UnauthorizedAccessGuard } from './unauthorized-access.guard';

describe('UnauthorizedAccessGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnauthorizedAccessGuard]
    });
  });

  it('should ...', inject([UnauthorizedAccessGuard], (guard: UnauthorizedAccessGuard) => {
    expect(guard).toBeTruthy();
  }));
});
