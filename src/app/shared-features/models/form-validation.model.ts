import { FormGroup, FormBuilder, Validators} from '@angular/forms';

export class FormValidation{
    registrationForm:FormGroup;
    loginForm:FormGroup;
    private formBuilder:FormBuilder = new FormBuilder();

    createRegistrationFormValidation(){
        this.registrationForm = this.formBuilder.group({
            'firstName':['',Validators.required],
            'lastName':['',Validators.required],
            'email':['',Validators.compose([Validators.required, Validators.email])],
            'password':['',Validators.compose([Validators.required,Validators.minLength(8)])]
        })
    }

    createLoginFormValidation(){
        this.loginForm = this.formBuilder.group({
            'email':['',Validators.required],
            'password':['',Validators.required]
        })
    }
}