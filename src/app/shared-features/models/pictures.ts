import { Avatar } from "../users/entities/avatar";
import { CoverPhoto } from "../users/entities/cover-photo";

export class Pictures {
    avatars:Avatar[];
    coverPhoto:CoverPhoto[];
}
