export class AuthenticationDetails{
    roles: string[];
    status:string;
    token:string;
    username:string;
}