import { Role } from './role';
import { Avatar } from './avatar';
export class User {
    id:number;
    firstName:string;
    lastName:string;
    username:string;
    password:string;
    email:string;
    ative:number;
    role:Role;
    avatar:Avatar;

}
