export class Avatar {
    id:number;
	filename:string;
    source:number;
    userId:number;
	type:string;
	size:number;
    createdOn:string;
    updatedOn:string;
}
