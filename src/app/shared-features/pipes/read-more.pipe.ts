import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'readMore'
})
export class ReadMorePipe implements PipeTransform {

  transform(value: string, length?: number): string {
    if(value != undefined && value != null && value.length > length){
      return value.substring(0,length+1)+" read more...";
    }
    return value;
  }

}
