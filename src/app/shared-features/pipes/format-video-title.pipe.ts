import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatVideoTitle'
})
export class FormatVideoTitlePipe implements PipeTransform {

  transform(stringValue: any, args?: any): any {
   if(stringValue.length>10){
    return stringValue.substring(1, 10)+"...";
   }
   return stringValue;
  }

}
