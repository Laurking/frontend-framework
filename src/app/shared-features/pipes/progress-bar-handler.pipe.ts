import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'progressBarHandler'
})
export class ProgressBarHandlerPipe implements PipeTransform {

  transform(percentage: any, args?: any): any {
    return "{width:"+percentage+"%}";
  }

}
