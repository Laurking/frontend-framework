import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imageEmbed'
})
export class ImageEmbedPipe implements PipeTransform {

  transform(source: any, type:string): any {
    if(type == null || type.length==0 || type == ''){
      return 'data:image/png;base64,' + source;
    }
    else{
      return 'data:image/'+type+';base64,' + source;
    }
  }

}
