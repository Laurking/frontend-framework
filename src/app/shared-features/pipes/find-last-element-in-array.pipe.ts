import { Pipe, PipeTransform } from '@angular/core';
import { Avatar } from '../users/entities/avatar';

@Pipe({
  name: 'getLastAvatar'
})
export class FindLastElementInArrayPipe implements PipeTransform {

  transform(avatars: Avatar[]): Avatar {
    if(avatars.length > 0){
      return avatars.reverse()[0];
    }
    return null;
  }

}
