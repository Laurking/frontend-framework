import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Pipe({
  name: 'previewVideoSanitizer'
})
export class PreviewVideoSanitizerPipe implements PipeTransform {
  constructor(private sanitiser:DomSanitizer){}

  transform(source: any, type:string): any {
    return this.sanitiser.bypassSecurityTrustResourceUrl(source);
  }

}
