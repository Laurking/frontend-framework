import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Pipe({
  name: 'videoSanitiser'
})
export class VideoSanitiserPipe implements PipeTransform {
  
  constructor(private sanitiser:DomSanitizer){}

  transform(source: any, type:string): any {
    if(type === null || type.length===0 || type === ''){
      return this.sanitiser.bypassSecurityTrustResourceUrl('data:video/mp4;base64,' + source);
    }
    else{
      return this.sanitiser.bypassSecurityTrustResourceUrl('data:video/'+type+';base64,' + source);
    }
  }

}
