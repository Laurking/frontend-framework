import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FeedRoutingModule } from './feed-routing-module';
import { SharedModuleModule } from '../../shared-features/modules/shared-module.module';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { HomeComponent } from './components/home/home.component';
import { FeedRootComponent } from './feed-root.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { SettingComponent } from './components/setting/setting.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ConnectionsComponent } from './components/connections/connections.component';
import { LeftSideBarComponent } from './components/left-side-bar/left-side-bar.component';
import { RightSideBarComponent } from './components/right-side-bar/right-side-bar.component';
import { PostService } from './services/post-service';
import { ImageService } from './services/image-service';
import { VideoService } from './services/video-service';
import { MainComponent } from './components/main/main.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { SearchComponent } from './components/search/search.component';
import { MoreSearchResultComponent } from './components/more-search-result/more-search-result.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { PicturesService } from '../../shared-features/services/pictures.service';
import { UserService } from '../../shared-features/services/user-service';
import { AuthGuard } from '../../shared-features/auth/auth.guard';
import { ProfilePicturesUpdatesComponent } from './components/profile-pictures-updates/profile-pictures-updates.component';
import { UpdateProfilePhotoComponent } from './components/update-profile-photo/update-profile-photo.component';
import { UpdateCoverPhotoComponent } from './components/update-cover-photo/update-cover-photo.component';
import { AvatarService } from '../../shared-features/services/avatar.service';
import { CoverPhotoService } from '../../shared-features/services/cover-photo.service';
import { CommentService } from './services/comment-service';
import { AboutmeComponent } from './components/aboutme/aboutme.component';
import { MyfeedsComponent } from './components/myfeeds/myfeeds.component';
import { MysettingComponent } from './components/mysetting/mysetting.component';
import { UpdatesComponent } from './components/updates/updates.component';
import { ReplyService } from './services/reply-service';


@NgModule({
  declarations: [
    HomeComponent,
    AboutComponent,
    ContactComponent,
    NavbarComponent,
    FeedRootComponent,
    NotificationsComponent,
    SettingComponent,
    ProfileComponent,
    ConnectionsComponent,
    LeftSideBarComponent,
    RightSideBarComponent,
    MainComponent,
    PostDetailsComponent,
    SearchComponent,
    MoreSearchResultComponent,
    SidebarComponent,
    ProfilePicturesUpdatesComponent,
    UpdateProfilePhotoComponent,
    UpdateCoverPhotoComponent,
    AboutmeComponent,
    MyfeedsComponent,
    MysettingComponent,
    UpdatesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FeedRoutingModule,
    SharedModuleModule
  ],
  providers: [AuthGuard, UserService,PicturesService, AvatarService, CommentService, CoverPhotoService,PostService,ImageService,ReplyService,VideoService],
  bootstrap: []
})
export class FeedAppModule { }
