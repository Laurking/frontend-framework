import { Injectable } from '@angular/core';
import { Urls } from '../../../shared-features/constants/urls';
import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { Image } from '../entities/image';
import { Post } from '../entities/post';

@Injectable()
export class VideoService {
  private parentUrl:string = Urls.getBaseUrl();
  private headers = new Headers({'content-type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  
  constructor(private httpService:Http) { }
  
  createVideo(post:Post, file:File){
    let formData = new FormData();
    formData.append('videoFile', file);
    var request = new XMLHttpRequest();
    request.open("POST", this.parentUrl+"/post/"+post.id+"/video/add-video",true);
    request.send(formData);
  }

  getAllVideos(){
    return this.httpService
    .get(this.parentUrl+"/post/read-all-posts",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }
  
  getVideo(imageId:number){
    return this.httpService
    .get(this.parentUrl+"/post/"+imageId+"/read-post",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }
  
  updateVideo(image:Image){
    return this.httpService
    .put(this.parentUrl+"/post/"+image.id+"/update-post",JSON.stringify(image),this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }
  
  deleteVideo(iamge:Image){
    return this.httpService
    .delete(this.parentUrl+"/post/"+iamge.id+"remove-post"+iamge.id,this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }
  
  errorHnadler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }
  
}
