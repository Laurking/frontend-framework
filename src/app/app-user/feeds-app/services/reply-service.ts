import { Injectable } from '@angular/core';
import { Urls } from '../../../shared-features/constants/urls';
import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import { Avatar } from '../../../shared-features/users/entities/avatar';
import { Reply } from '../entities/reply';

@Injectable()
export class ReplyService {
  private parentUrl:string = Urls.getBaseUrl();
  private headers = new Headers({'content-type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  
  constructor(private httpService:Http) { }
  
  createReply(reply:Reply):Observable<Reply>{
    return this.httpService
    .post(this.parentUrl+"/comment/"+reply.comment.id+"/reply/add-reply",JSON.stringify(reply),this.options)
    .map((response:Response)=>response.json())
    .flatMap((newReply: Reply) => {
      return Observable.forkJoin([
        this.httpService.get(this.parentUrl+"users/"+reply.userId,this.options)
        .map((response:Response)=>response.json()),
        this.httpService.get(this.parentUrl+"users/user/"+newReply.userId+"/avartar/findAvatars",this.options)
        .map((response:Response)=>response.json())
      ]).map((data: any) => {
        newReply.user = data[0];
        let avatar:Avatar[] = data[1];
        newReply.avatar = avatar.reverse()[0];  // need to get avatar from reply on the server side
        return newReply;
      });
    })
  }

  // createComment(comment:Comment):Observable<Comment>{
  //   return this.httpService
  //   .post(this.parentUrl+"/post/"+comment.post.id+"/comment/add-comment",JSON.stringify(comment),this.options)
  //   .map((response:Response)=>response.json())
  //   .catch(this.errorHnadler);
  // }
  
  errorHandler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }
  
}
