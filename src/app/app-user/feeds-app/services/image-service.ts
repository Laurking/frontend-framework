import { Injectable } from '@angular/core';
import { Urls } from '../../../shared-features/constants/urls';
import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import { Image } from '../entities/image';
import { Post } from '../entities/post';

@Injectable()
export class ImageService {
  private parentUrl:string = Urls.getBaseUrl();
  private headers = new Headers({'content-type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  
  constructor(private httpService:Http) { }
  
  createImage(post:Post, file:File):Observable<Image>{
    return Observable.fromPromise(new Promise((resolve, reject) => {
      let formData: any = new FormData()
      let request = new XMLHttpRequest()
      formData.append('imageFiles', file);
      request.onreadystatechange = function () {
        if (request.readyState === 4) {
          if (request.status === 200) {
            resolve(JSON.parse(request.response))
          } else {
            reject(request.response)
          }
        }
      }
      request.open("POST", this.parentUrl+"/post/"+post.id+"/image/add-image",true);
      request.send(formData)
    }));
  }
    
  getAllImages(){
    return this.httpService
    .get(this.parentUrl+"/post/read-all-posts",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }
  
  getImage(imageId:number){
    return this.httpService
    .get(this.parentUrl+"/post/"+imageId+"/read-post",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }
  
  updateImage(image:Image){
    return this.httpService
    .put(this.parentUrl+"/post/"+image.id+"/update-post",JSON.stringify(image),this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }
  
  deleteImage(iamge:Image){
    return this.httpService
    .delete(this.parentUrl+"/post/"+iamge.id+"remove-post"+iamge.id,this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }
  
  errorHnadler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }
  
}
