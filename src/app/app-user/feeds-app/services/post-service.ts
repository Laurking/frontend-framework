import { Injectable } from '@angular/core';
import { Urls } from '../../../shared-features/constants/urls';
import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import { Post } from '../entities/post';
import { Image } from '../entities/image';

@Injectable()
export class PostService {
  private parentUrl:string = Urls.getBaseUrl();
   private headers = new Headers({'content-type':'application/json'});
   private options = new RequestOptions({headers:this.headers});

  constructor(private httpService:Http) { }


  createPost(post:Post, file:File):Observable<Post>{
    if(file == null){
     return this.createOnlyPost(post);
    }
    else{
      return this.createPostWithPictures(post,file);
    }

  }
  
  private createOnlyPost(post:Post):Observable<Post>{
    return this.httpService
    .post(this.parentUrl+"/post/add-post",JSON.stringify(post),this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }


  private createPostWithPictures(post:Post,file): Observable<Post> {
    return this.httpService
    .post(this.parentUrl+"/post/add-post",JSON.stringify(post),this.options)
      .map((res: any) => res.json())
      .flatMap((post:Post) => {
          return Observable.fromPromise(new Promise((resolve, reject) => {
            let formData: any = new FormData()
            let request = new XMLHttpRequest()
            formData.append('imageFiles', file);
            request.onreadystatechange = function () {
              if (request.readyState === 4) {
                if (request.status === 200) {
                  let images:Image[] = new Array();
                  let image:Image = JSON.parse(request.response);
                  images.push(image);
                  post.images = images;
                  resolve(post);
                  
                } else {
                  reject(request.response)
                }
              }
            }
            request.open("POST", this.parentUrl+"/post/"+post.id+"/image/add-image",true);
            request.send(formData)
          }));
      }).catch(this.errorHnadler);;
  }
  // createPost(post:Post){
  //   return this.httpService
  //   .post(this.parentUrl+"/post/add-post",JSON.stringify(post),this.options)
  //   .map((response:Response)=>response.json())
  //   .catch(this.errorHnadler);
  // }

  getAllPost(){
    return this.httpService
    .get(this.parentUrl+"/post/read-all-posts",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  getPost(postId:number){
    return this.httpService
    .get(this.parentUrl+"/post/"+postId+"/read-post",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  updatePost(post:Post){
    return this.httpService
    .put(this.parentUrl+"/post/"+post.id+"/update-post",JSON.stringify(post),this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  deletePost(postId:number){
    return this.httpService
    .delete(this.parentUrl+"/post/"+postId+"/remove-post",this.options)
    .map((response:Response)=>response.json())
    .catch(this.errorHnadler);
  }

  errorHnadler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }

}
