import { Injectable } from '@angular/core';
import { Urls } from '../../../shared-features/constants/urls';
import {Http, Headers, RequestOptions,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import { Comment } from '../entities/comment';
import { Avatar } from '../../../shared-features/users/entities/avatar';

@Injectable()
export class CommentService {
  private parentUrl:string = Urls.getBaseUrl();
  private headers = new Headers({'content-type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  
  constructor(private httpService:Http) { }
  
  createComment(comment:Comment):Observable<Comment>{
    return this.httpService
    .post(this.parentUrl+"/post/"+comment.post.id+"/comment/add-comment",JSON.stringify(comment),this.options)
    .map((response:Response)=>response.json())
    .flatMap((newComment: Comment) => {
      return Observable.forkJoin([
        this.httpService.get(this.parentUrl+"users/"+comment.userId,this.options)
        .map((response:Response)=>response.json()),
        this.httpService.get(this.parentUrl+"users/user/"+comment.userId+"/avartar/findAvatars",this.options)
        .map((response:Response)=>response.json())
      ]).map((data: any) => {
        newComment.user = data[0];
        let avatar:Avatar[] = data[1];
        newComment.avatar = avatar.reverse()[0];
        return newComment;
      });
    })
  }

  // createComment(comment:Comment):Observable<Comment>{
  //   return this.httpService
  //   .post(this.parentUrl+"/post/"+comment.post.id+"/comment/add-comment",JSON.stringify(comment),this.options)
  //   .map((response:Response)=>response.json())
  //   .catch(this.errorHnadler);
  // }
  
  errorHandler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }
  
}
