import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FeedRootComponent } from './feed-root.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { MainComponent } from './components/main/main.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { MoreSearchResultComponent } from './components/more-search-result/more-search-result.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AuthGuard } from '../../shared-features/auth/auth.guard';
import { ProfilePicturesUpdatesComponent } from './components/profile-pictures-updates/profile-pictures-updates.component';
import { UpdateProfilePhotoComponent } from './components/update-profile-photo/update-profile-photo.component';
import { UpdateCoverPhotoComponent } from './components/update-cover-photo/update-cover-photo.component';
import { AboutmeComponent } from './components/aboutme/aboutme.component';
import { MyfeedsComponent } from './components/myfeeds/myfeeds.component';
import { MysettingComponent } from './components/mysetting/mysetting.component';
import { UpdatesComponent } from './components/updates/updates.component';


const routes : Routes =[
  {
    path:'marialex',
    component:FeedRootComponent,
    canActivate:[AuthGuard],
    children:[
      { path:'feed',component:MainComponent,
      children:[{path:'',component:HomeComponent}]},

      { path:'about',component:AboutComponent},

      {path:'post/:id/details',component:MainComponent,
      children:[{path:'',component:PostDetailsComponent}]},

      {path:'more-search-result',component:MainComponent,
      children:[{path:'',component:MoreSearchResultComponent}]},

      {path:"aboutme",component:ProfileComponent,children:[{path:'',component:AboutmeComponent}]},

      {path:"myfeeds",component:ProfileComponent,children:[{path:'',component:MyfeedsComponent}]},

      {path:"mysetting",component:ProfileComponent,children:[{path:'',component:MysettingComponent}]},
      
      { path:'contact',component:ContactComponent},
      { path:'profile',pathMatch:'full' , redirectTo:'aboutme'},
      
      { path:'update/coverphoto',component:UpdatesComponent,
      children:[{path:'',component:UpdateCoverPhotoComponent}]},

      { path:'update/profilephoto',component:UpdatesComponent,
      children:[{path:'',component:UpdateProfilePhotoComponent}]}
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})

export class FeedRoutingModule { }
