import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FeedRootComponent } from './feed-root.component';


describe('FarmRootComponent', () => {
  let component: FeedRootComponent;
  let fixture: ComponentFixture<FeedRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
