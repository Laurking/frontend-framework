import { Post } from "./post";

export class Image{
    id:number;
	filename:string;
	source:number;
	type:string;
	size:number;
	userId:number;
    createdOn:string;
    updatedOn:string;
	post:Post;

    
}