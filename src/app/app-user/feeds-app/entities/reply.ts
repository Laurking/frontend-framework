import { Comment } from "./comment";
import { User } from "../../../shared-features/users/entities/user";
import { Avatar } from "../../../shared-features/users/entities/avatar";

export class Reply{
    id:number;
	body:string;
    userId:string;
    user:User;
    avatar:Avatar;
    createdOn:string;
    updatedOn:string;
    comment:Comment;
}