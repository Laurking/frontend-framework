import { Post } from "./post";
import { Reply } from "./reply";
import { User } from "../../../shared-features/users/entities/user";
import { Avatar } from "../../../shared-features/users/entities/avatar";

export class Comment{
    id:number;
	body:string;
	userId:number;
    createdOn:string;
    updatedOn:string;
    post:Post;
    user:User;
    avatar:Avatar;
    replies:Reply[];

}