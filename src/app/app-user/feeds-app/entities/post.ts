import { Video } from "./video";
import { Image } from "./image";
import { Comment } from "./comment";
import { User } from "../../../shared-features/users/entities/user";
import { Avatar } from "../../../shared-features/users/entities/avatar";

export class Post{
	id:number;
	body:string;
	userId:number;
    createdOn:string;
    updatedOn:string;
	images:Image[];
	avatar:Avatar;
	user:User;
	video:Video;
	comments:Comment[];

}