import { ElementRef, ViewChild, Component, OnInit, OnDestroy, HostListener} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { Router } from '@angular/router';
import { PostService } from '../../services/post-service';
import { Post } from '../../entities/post';

@Component({
  selector: 'feed-app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [
    trigger('animateSideBar', [
      state('in', style({
        marginRight: '190px'
      })),
      state('out', style({
        marginRight: '-190px'
      })),
      transition('in => out', animate('200ms ease-in-out')),
      transition('out => in', animate('200ms ease-in-out'))
    ])
  ]
})
export class NavbarComponent implements OnInit{
  @ViewChild('sidebar') sidebar:ElementRef;
  menuState:string = 'out';
  
  constructor(private router:Router) { 

  }
  
  ngOnInit() {
    this.menuState = 'out';
  }

 
  toggleSideBar() {
    if(this.menuState == 'in'){
      this.menuState = 'out';
    }
    else{
      this.menuState = 'in';
    }
    this.sidebar.nativeElement.style.display = "block";
  }

  @HostListener('window:keydown', ['$event', '$event.target'])
  keyboardInput(event: KeyboardEvent) {
    let targets = []
    if(event.keyCode == 27){
      this.menuState = 'out';
    }
  }

  @HostListener('window:click', ['$event', '$event.target'])
  mouseEvent(event) {
    let targets = ["menuIcon"];
    let target = event.target.parentNode.parentNode.getAttribute('id');
    if(target == null || (target != null && targets.indexOf(target)!=0)){
      this.menuState = 'out';
    }
  }
}
