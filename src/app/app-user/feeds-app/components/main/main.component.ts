import { Component, OnInit, ViewChild} from '@angular/core';
import { PostService } from '../../services/post-service';
import { Post } from '../../entities/post';
import { ImageService } from '../../services/image-service';
import { VideoService } from '../../services/video-service';


@Component({
  selector: 'main-component',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  private previewImageUrl:string="";
  private previewVideoUrl:string="";
   private posts:Post[] = new Array();
   private postToSend:Post = new Post();
   private postReceived:Post = new Post();
   private fileData:File;
  

  constructor(private postService:PostService, 
    private imageService:ImageService,
    private videoService:VideoService) { }
  
  ngOnInit() {
    this.getAllPost();
  }
  
  private threadGetAllPost(){
  //     setInterval(function() {
  //       this.postService.getAllPost().subscribe(data => {
  //         this.posts = data;
  //         console.log(this.posts);
  //       },error =>{
  //         console.log(error);
  //       })
  //  }, 100);
  }

  private getAllPost(){
    this.postService.getAllPost().subscribe(data => {
      this.posts = data;
    },error =>{
      console.log(error);
    })
  }

  createPost(){
  if(this.postToSend != undefined || this.fileData != undefined){
    if((this.postToSend.body !=null && this.postToSend.body.length>1) && this.fileData==undefined){
      this.postService.createPost(this.postToSend, this.fileData).subscribe(data=>{
        this.postReceived = data;
         this.posts.push(this.postReceived);
      },error =>{
        console.log(error);
      })
    }
    else{
      this.postService.createPost(this.postToSend, this.fileData).subscribe(data=>{
        this.postReceived = data;
        this.serviceFactory(this.postReceived,this.fileData);
         this.posts.push(this.postReceived);
      },error =>{
        console.log(error);
      })
      
    }
    this.postToSend = new Post();
  }
  }


  loadImage(event){
    var image = event.target.files;
    if (image && image[0]) {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.previewImageUrl = event.target.result;
        this.previewVideoUrl ="";
      }
      this.fileData = image[0];
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  loadVideo(event){
    var video = event.target.files;
    if (video && video[0]) {
      this.previewVideoUrl = window.URL.createObjectURL(video[0]);
      this.fileData = video[0];
      this.previewImageUrl = "";
    }
  }

  cancelVideoUpload(){
    this.previewVideoUrl="";
    this.fileData = null;
  }
  cancelImageUpload(){
    this.previewImageUrl="";
    this.fileData = null;
  }

  private serviceFactory(post:Post, file:File){
    let imagesArray = ["png","jpeg","jpg","bmp","gif"];
    let videosArray = ["mp4","mp3","mp2"];
    if(file != null){
      let fileType = file.type.toLocaleLowerCase().split("/").pop();
      if(fileType != null && fileType.length>=3){
        if(imagesArray.indexOf(fileType)>=0){
          this.imageService.createImage(post,file);
        }
        if(videosArray.indexOf(fileType)>=0){
          console.log("Video array");
          this.videoService.createVideo(post,file);
        }
      }
    }
    
    
  }

}