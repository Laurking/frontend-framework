import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCoverPhotoComponent } from './update-cover-photo.component';

describe('UpdateCoverPhotoComponent', () => {
  let component: UpdateCoverPhotoComponent;
  let fixture: ComponentFixture<UpdateCoverPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCoverPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCoverPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
