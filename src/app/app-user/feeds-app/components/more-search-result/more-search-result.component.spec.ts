import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoreSearchResultComponent } from './more-search-result.component';

describe('MoreSearchResultComponent', () => {
  let component: MoreSearchResultComponent;
  let fixture: ComponentFixture<MoreSearchResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoreSearchResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoreSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
