import { ElementRef, ViewChild, Component, OnInit, OnDestroy, HostListener} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { Router } from '@angular/router';
import { PostService } from '../../services/post-service';
import { Post } from '../../entities/post';

@Component({
  selector: 'app-more-search-result',
  templateUrl: './more-search-result.component.html',
  styleUrls: ['./more-search-result.component.scss']
})
export class MoreSearchResultComponent implements OnInit {
 
  private searchResultContainer:Post[] = new Array();
  private sub: any;
  
  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private postService:PostService,
  ) { 
   
  }
  
  ngOnInit() {
    this.sub = this.postService.getAllPost().subscribe(data=>{
      this.searchResultContainer = data;
    },error=>{
      console.log(error);
    }) 
   
  }
  
  ngOnDestroy(){
    this.sub.unsubscribe();
  }
  
  
  

  
  // @HostListener('window:keydown', ['$event'])
  // keyboardInput(event: KeyboardEvent) {
  //   let targets = []
  //   if(event.keyCode == 27){
  //     this.searchResults.nativeElement.style.display="none";
  //     this.searchBox.nativeElement.blur()
  //     this.searchValues="";
  //   }
  // }
  
  // @HostListener('document:click', ['$event'])
  // mouseEvent(event) {
  //   let targets = ["search-result","search-box"];
  //   if(event.srcElement.offsetParent != null 
  //     && event.srcElement != null){ 
  //       if(targets.indexOf(event.srcElement.offsetParent.getAttribute('id'))<0
  //       && targets.indexOf(event.srcElement.getAttribute('id'))<0){
  //         this.searchResults.nativeElement.style.display="none";
  //       }   
  //     }
  //   }

}
