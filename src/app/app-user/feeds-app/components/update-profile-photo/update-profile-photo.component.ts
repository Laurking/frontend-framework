import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { AvatarService } from '../../../../shared-features/services/avatar.service';

@Component({
  selector: 'app-update-profile-photo',
  templateUrl: './update-profile-photo.component.html',
  styleUrls: ['./update-profile-photo.component.scss']
})
export class UpdateProfilePhotoComponent implements OnInit {

  @ViewChild('inputFiles') inputFiles;
  @ViewChild("progressBar") progressBar;
  private files:File[] = new Array();
  private uploadStatus:string;
  private fileUPloadStatus:string;
  progress: { percentage: number } = { percentage: 0 }
  
  private currentUser = JSON.parse(sessionStorage.getItem('currentUser'));

  constructor(private avatarService:AvatarService, private router:Router) {
    
   }
  
  ngOnInit() {
    this.uploadStatus = "upload";
    this.fileUPloadStatus="upload";

    
  }
  
  changeUploadStatus(){
    this.uploadStatus = "upload";
    this.fileUPloadStatus="upload";
    this.router.navigate(['/marialex/video/upload']);
    
  }

  handleFiles(){
    this.inputFiles.nativeElement.parentNode.style.background="#CCCCCC";
  }
  
  onDragOver(event) {
    event.preventDefault();
    event.stopPropagation();
    this.inputFiles.nativeElement.parentNode.style.background="#CCCCCC";
    
  }
  
  onDragLeave(event) {
    event.preventDefault();
    event.stopPropagation();
    this.inputFiles.nativeElement.parentNode.style.background="#FFFFFF";
    
  }
  
  
  onDrop(event: any) {
    event.preventDefault();
    event.stopPropagation();
    let dropBoxFiles:FileList = event.dataTransfer.files;
    Array.from(dropBoxFiles).forEach(file => {
      if(file!=null){
        this.files.push(file);
        console.log(this.files);
      }
    });
    
  }
  
  uploadFiles(){
    let inputData:FileList = this.inputFiles.nativeElement.files;
    Array.from(inputData).forEach(file => {
      if(file!=null){
        this.files.push(file);
      }
    });

    if(this.files != null){
      this.avatarService.addAvatar(this.currentUser.id, this.files).subscribe(event=>{
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
          this.uploadStatus="uploading";
          this.fileUPloadStatus="uploading";
        } else if (event instanceof HttpResponse) {
          this.fileUPloadStatus="completed";
          this.router.navigate(['/marialex/feed/profile'])
        }
      },error=>{
        console.log(error);
      })
   }
    
  }
  

}
