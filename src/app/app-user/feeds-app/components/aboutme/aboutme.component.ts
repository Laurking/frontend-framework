import { Component, OnInit } from '@angular/core';
import { User } from '../../../../shared-features/users/entities/user';

@Component({
  selector: 'app-aboutme',
  templateUrl: './aboutme.component.html',
  styleUrls: ['./aboutme.component.scss']
})
export class AboutmeComponent implements OnInit {

  private currentUser:User = new User();

  constructor() { }

  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
  }


}
