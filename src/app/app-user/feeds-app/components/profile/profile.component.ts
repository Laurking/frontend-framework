import { ElementRef,Component,ViewChild, OnInit, OnDestroy, AfterViewChecked, AfterViewInit } from '@angular/core';
import { User } from '../../../../shared-features/users/entities/user';
import { Avatar } from '../../../../shared-features/users/entities/avatar';
import { AvatarService } from '../../../../shared-features/services/avatar.service';
import { CoverPhoto } from '../../../../shared-features/users/entities/cover-photo';
import { CoverPhotoService } from '../../../../shared-features/services/cover-photo.service';
import { PicturesService } from '../../../../shared-features/services/pictures.service';
import { Pictures } from '../../../../shared-features/models/pictures';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, AfterViewInit, OnDestroy{
  
  @ViewChild('updateProfileImage') updateProfileImage:ElementRef;
  private currentUser:User = new User();
  private avatar:Avatar = new Avatar();
  private coverPhoto:CoverPhoto = new CoverPhoto();
  private avatarSubscriber:any;
  private coverPhotoSubscriber:any;
  private picturesServiceSubs:any;
  
  constructor(private picturesService:PicturesService) { }
  
  ngOnInit() {
    
    
    // this.coverPhotoSubscriber = this.coverPhotoService.getCoverPhotoByUserId(this.currentUser.id).subscribe(data =>{
    //   let coverPhotos:CoverPhoto[] = data;
    //   this.coverPhoto = coverPhotos.reverse()[0];
    // },error =>{
    //   console.log(error);
    // })
    // this.avatarSubscriber = this.avatarService.getAvatarsByUserId(this.currentUser.id).subscribe(data =>{
    //   let avatars:Avatar[] = data;
    //   this.avatar = avatars.reverse()[0];
    // },error =>{
    //   console.log(error);
    // })
    
  }
  
  ngAfterViewInit(){
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    this.picturesServiceSubs = this.picturesService.findPicturesByUserId(this.currentUser.id).subscribe(data =>{
      let pictures:Pictures = data;
      this.avatar = pictures.avatars.reverse()[0];
      this.coverPhoto = pictures.coverPhoto.reverse()[0];
    },error =>{
      console.log(error);
    })
  }
  ngOnDestroy(){
    // this.avatarSubscriber.unsubscribe();
    // this.coverPhotoSubscriber.unsubscribe();
    this.picturesServiceSubs.unsubscribe();
  }
  updateAvatar(){
    this.updateProfileImage.nativeElement.style.display = "block";
  }
  
  closeProfileUpdate(){
    this.updateProfileImage.nativeElement.style.display = "none";
  }
}
