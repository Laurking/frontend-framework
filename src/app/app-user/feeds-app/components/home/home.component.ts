import { Component, OnInit, ViewChild, ElementRef, HostListener} from '@angular/core';
import { PostService } from '../../services/post-service';
import { Post } from '../../entities/post';
import { ImageService } from '../../services/image-service';
import { VideoService } from '../../services/video-service';
import { Image } from '../../entities/image';
import { User } from '../../../../shared-features/users/entities/user';
import { CommentService } from '../../services/comment-service';
import { Comment } from '../../entities/comment';
import { post } from 'selenium-webdriver/http';
import { Reply } from '../../entities/reply';
import { ReplyService } from '../../services/reply-service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('sensitiveAction') sensitiveAction:ElementRef;
  private previewImageUrl:string="";
  private previewVideoUrl:string="";
  private posts:Post[] = new Array();
  private postToSend:Post = new Post();
  private postReceived:Post = new Post();
  private imageReceived:Image[] = new Array();
  private commentStub:any;
  private replyStub:any;
  private fileData:File;
  private loading:boolean;
  private currentUser:User =  this.currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
  
  constructor(private postService:PostService, 
    private imageService:ImageService,
    private videoService:VideoService, 
    private commentService:CommentService,
    private replyService:ReplyService) { }
    
    ngOnInit() {
      this.getAllPost();
    }
    
    ngOnDestroy(){
      //this.commentStub.unsubscribe();
      this.replyStub.unsubscribe();
    }
    
    private getAllPost(){
      this.loading = true;
      this.postService.getAllPost().subscribe(data => {
        this.posts = data;
        this.posts = this.posts.reverse();
        this.loading = false;
      },error =>{
        console.log(error);
      })
    }
    createPost(){
      if(!this.postToSend.body && !this.fileData) return;
      this.postToSend.userId =this.currentUser.id;
      this.postService.createPost(this.postToSend,this.fileData).subscribe(data=>{
        this.postReceived = data;
        this.postReceived.user = this.currentUser;
        this.posts.unshift(this.postReceived);
        this.previewImageUrl = "";
        this.previewVideoUrl = "";
        this.postToSend.body = "";
      },error =>{
        console.log(error);
      })
    }
    // createPost(){
    //   if(this.postToSend != undefined || this.fileData != undefined){
    //     if((this.postToSend.body !=null && this.postToSend.body.length>1) && this.fileData==undefined){
    //       this.postService.createPost(this.postToSend).subscribe(data=>{
    //         this.postReceived = data;
    //         this.posts.push(this.postReceived);
    //       },error =>{
    //         console.log(error);
    //       })
    //     }
    //     else{
    //       this.postService.createPost(this.postToSend).subscribe(data=>{
    //         this.postReceived = data;
    //         this.imageService.createImage(this.postReceived,this.fileData).subscribe(data =>{
    //           let image:Image = data;
    //           this.imageReceived.push(image);
    //           this.postReceived.images = this.imageReceived;
    //           this.posts.push(this.postReceived);
    
    //         },error =>{
    //           console.log(error);
    //         })
    //       },error =>{
    //         console.log(error);
    //       })
    
    //     }
    //     this.postToSend = new Post();
    //   }
    // }
    
    closeSensitiveAction(id:number){
      let element = document.getElementsByClassName("sensitiveAction"+id);
      element[0].classList.add("sensitiveActionHidden")
      
    }
    displaySensitiveActions(id:number){
      var elems = Array.from(document.querySelectorAll(".sensitiveAction"));
      elems.forEach(elem => 
        elem.classList.add("sensitiveActionHidden")
      );
      let element = document.getElementsByClassName("sensitiveAction"+id);
      element[0].classList.remove("sensitiveActionHidden")
    }
    loadImage(event){
      var image = event.target.files;
      if (image && image[0]) {
        var reader = new FileReader();
        reader.onload = (event:any) => {
          this.previewImageUrl = event.target.result;
          this.previewVideoUrl ="";
        }
        this.fileData = image[0];
        reader.readAsDataURL(event.target.files[0]);
      }
    }
    
    loadVideo(event){
      var video = event.target.files;
      if (video && video[0]) {
        this.previewVideoUrl = window.URL.createObjectURL(video[0]);
        this.fileData = video[0];
        this.previewImageUrl = "";
      }
    }
    
    cancelVideoUpload(){
      this.previewVideoUrl="";
      this.fileData = null;
    }
    cancelImageUpload(){
      this.previewImageUrl="";
      this.fileData = null;
    }
    
    
    onSubmit(comment:Comment){ // adding comment
      this.commentStub = this.commentService.createComment(comment).subscribe(data =>{
        let newComment:Comment = data;
        var index = this.posts.indexOf(comment.post);
        var post:Post = comment.post;
        post.comments.push(newComment);
        post.comments.reverse();
        this.posts[this.posts.indexOf(comment.post)] = post;
      },error =>{
        console.log(error);
      })
    }
    
    addreplyToComment(reply:Reply){
      this.replyStub = this.replyService.createReply(reply).subscribe(data =>{
        let reply:Reply = data;
        console.log(reply);
      },error =>{
        console.log(error);
      })
    }
    
    toggleCommentBoxByValue(id:string){  // toggle comment by value
      Array.from(document.querySelectorAll(".commentId")).forEach(elem => 
        elem.classList.add("hidden")
      );
      Array.from(document.querySelectorAll(".addComments")).forEach(elem => 
        elem.classList.add("hidden")
      );
      let element = document.getElementById(id);
      element.classList.remove("hidden");
    }

    toggleReplyBoxByValue(id:string){
      Array.from(document.querySelectorAll(".addComments")).forEach(elem => 
        elem.classList.add("hidden")
      );
      Array.from(document.querySelectorAll(".commentId")).forEach(elem => 
        elem.classList.add("hidden")
      );
      let element = document.getElementById(id);
      element.classList.remove("hidden");
    }
    
    private serviceFactory(post:Post, file:File){
      let imagesArray = ["png","jpeg","jpg","bmp","gif"];
      let videosArray = ["mp4","mp3","mp2"];
      if(file != null){
        let fileType = file.type.toLocaleLowerCase().split("/").pop();
        if(fileType != null && fileType.length>=3){
          if(imagesArray.indexOf(fileType)>=0){
            return this.imageService.createImage(post,file);
          }
          if(videosArray.indexOf(fileType)>=0){
            console.log("Video array");
            return this.videoService.createVideo(post,file);
          }
          return null;
        }
        return null;
      }
      return null;
    }
    
    cancelDeleteAction(id:number){
      let elementToDelete = document.getElementsByClassName("confirmDeleteActionFor"+id);
      elementToDelete[0].classList.remove("confirmDeleteActionForShow");
    }
    
    deletePostAction(id:number){
      let closeSideMenuPopUp = document.getElementsByClassName("sensitiveAction"+id);
      closeSideMenuPopUp[0].classList.add("sensitiveActionHidden")
      let elementToDelete = document.getElementsByClassName("confirmDeleteActionFor"+id);
      elementToDelete[0].classList.add("confirmDeleteActionForShow");
    }
    
    deletePost(id:number){
      this.postService.deletePost(id).subscribe(data =>{
        let post:Post = data;
        var index = this.posts.indexOf(post);
        if (index > -1) {
          this.posts.splice(index, 1);
        }
        let elementToDelete = document.getElementsByClassName("globalPost"+id);
        elementToDelete[0].classList.add("hidePost");
      },error =>{
        console.log(error);
      })
    }
    @HostListener('window:click', ['$event', '$event.target'])
    mouseEvent(event) {
      let targets = ["dotMenu"];
      let toggleButton = event.target.getAttribute("class") || event.target.parentNode.getAttribute("class");
      if((toggleButton!=null && targets.indexOf(toggleButton)==-1)){
        let elementToDelete = Array.from(document.getElementsByClassName("sensitiveAction"));
          elementToDelete.forEach(elem => 
            elem.classList.add("sensitiveActionHidden")
        );
      }
      
    }
  }