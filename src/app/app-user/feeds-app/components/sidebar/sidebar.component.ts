import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../shared-features/services/user-service';
import { Router } from '@angular/router';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(private router:Router, private userService:UserService) { }

  ngOnInit() {
  }

  logout(){
    this.userService.logout().subscribe(data =>{
      localStorage.removeItem('token')
      this.router.navigate(['/marialex/login'])
    },error =>{
      console.log(error);
    })
  }

}
