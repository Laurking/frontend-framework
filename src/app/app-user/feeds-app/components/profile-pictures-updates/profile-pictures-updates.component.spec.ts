import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilePicturesUpdatesComponent } from './profile-pictures-updates.component';

describe('ProfilePicturesUpdatesComponent', () => {
  let component: ProfilePicturesUpdatesComponent;
  let fixture: ComponentFixture<ProfilePicturesUpdatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilePicturesUpdatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePicturesUpdatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
