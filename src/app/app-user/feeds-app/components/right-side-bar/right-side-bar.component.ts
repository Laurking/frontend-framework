import { Component, OnInit } from '@angular/core';
import { User } from '../../../../shared-features/users/entities/user';
import { Avatar } from '../../../../shared-features/users/entities/avatar';
import { AvatarService } from '../../../../shared-features/services/avatar.service';

@Component({
  selector: 'right-side-bar',
  templateUrl: './right-side-bar.component.html',
  styleUrls: ['./right-side-bar.component.scss']
})
export class RightSideBarComponent implements OnInit {
  private currentUser:User = new User();
  private avatar:Avatar = new Avatar();
  constructor(private avatarService:AvatarService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    this.avatarService.getAvatarsByUserId(this.currentUser.id).subscribe(data =>{
      let avatars:Avatar[] = data;
      this.avatar = avatars.reverse()[0];
    },error =>{
      console.log(error);
    })
  }

}
