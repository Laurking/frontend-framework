import { ElementRef, ViewChild, Component, OnInit, OnDestroy, HostListener} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { Router } from '@angular/router';
import { PostService } from '../../services/post-service';
import { Post } from '../../entities/post';


@Component({
  selector: 'feeds-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  @ViewChild('sidebar') sidebar:ElementRef;
  @ViewChild('searchBox') searchBox:ElementRef;
  @ViewChild('searchResults') searchResults:ElementRef;
  private searchResultContainer:Post[] = new Array();
  private foundsResults:Post[] = new Array();
  private searchValues:string = "";
  private sub: any;
  
  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private postService:PostService,
  ) { 
   
  }
  
  ngOnInit() {
    this.sub = this.postService.getAllPost().subscribe(data=>{
      this.searchResultContainer = data;
    },error=>{
      console.log(error);
    }) 
   
  }
  
  ngOnDestroy(){
    this.sub.unsubscribe();
  }
  
  gotoLink(){
   this.searchResults.nativeElement.style.display="none";
  }
  onSearchBoxFocus(){
    this.searchResults.nativeElement.style.display="block";
  }
  
  onSearchStart(){
    this.foundsResults = this.findPostsOrPeople();
  }
  
  private findPostsOrPeople(){
    let posts:Post[] = new Array();
    this.searchValues = this.searchValues.toLocaleLowerCase();
    if(this.searchValues != undefined && this.searchValues!=null && this.searchValues!="" && this.searchValues.length>=1){
      for(let post of this.searchResultContainer){
        if((post.body != undefined || post.body != null )&& (post.body.toLocaleLowerCase().startsWith(this.searchValues) 
        || post.body.toLocaleLowerCase().endsWith(this.searchValues)
        || post.body.toLocaleLowerCase().includes(this.searchValues))){
          posts.push(post);
        }
      }
    }
    return posts;
  }
  
  @HostListener('window:keydown', ['$event', '$event.target'])
  keyboardInput(event: KeyboardEvent) {
    let targets = []
    if(event.keyCode == 27){
      this.searchResults.nativeElement.style.display="none";
      this.searchBox.nativeElement.blur()
      this.searchValues="";
    }
  }
  
  @HostListener('document:click', ['$event', '$event.target'])
  mouseEvent(event) {
    let targets = ["search-result","search-box"];
    if(event.target.offsetParent != null 
      && event.target != null){ 
        if(targets.indexOf(event.target.offsetParent.getAttribute('id'))<0
        && targets.indexOf(event.target.getAttribute('id'))<0){
          this.searchResults.nativeElement.style.display="none";
        }   
      }
    }
  }
  