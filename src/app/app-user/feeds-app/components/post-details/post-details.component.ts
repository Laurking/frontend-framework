import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute,NavigationStart,NavigationEnd,RoutesRecognized,Router} from '@angular/router';
import {Location} from '@angular/common';
import { Post } from '../../entities/post';
import { PostService } from '../../services/post-service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit,OnDestroy{
  private post:Post = new Post();
  private sub: any;
  private stub:any;
  
  constructor(private postServide:PostService,
    private route: ActivatedRoute,
    private location: Location,
    private router:Router) {}
    
    ngOnInit() {
      this.sub = this.route.params.subscribe(params => {
        const id = +params['id']; 
        this.sub = this.postServide.getPost(id).subscribe(data=>{
          this.post = data;
        },error=>{
          console.log(error);
        })
      });
     
      this.stub = this.router.events.subscribe(event =>{
        if(event instanceof NavigationStart){
          const url = this.router.url;
          this.location.replaceState(url);
        }
      })
    }
    
    ngOnDestroy() {
      this.sub.unsubscribe();
      this.stub.unsubscribe();
    }
    
  }
  