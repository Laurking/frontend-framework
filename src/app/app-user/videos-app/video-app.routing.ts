import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideoAppComponent } from './video-app.component';
import { HomeComponent } from './components/home/home.component';
import { UploadVideoComponent } from './components/upload-video/upload-video.component';
import { ProfileComponent } from './components/profile/profile.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { ChannelsComponent } from './components/channels/channels.component';
import { ConnectionsComponent } from './components/connections/connections.component';
import { SearchComponent } from './components/search/search.component';
import { AboutUserComponent } from './components/about-user/about-user.component';
import { UsersVideosComponent } from './components/users-videos/users-videos.component';
import { UsersChannelsComponent } from './components/users-channels/users-channels.component';
import { SettingComponent } from './components/setting/setting.component';
import { VideoPlayerComponent } from './components/video-player/video-player.component';
import { AuthGuard } from '../../shared-features/auth/auth.guard';


const routes: Routes = [
  { path:'marialex/video',
  component:VideoAppComponent,
  canActivate:[AuthGuard],
  children:[
    { path:'',component:HomeComponent},
    { path:'upload',component:UploadVideoComponent},
    { path:'notifications',component:NotificationsComponent},
    { path:'search',component:SearchComponent},
    { path:'subscriptions',component:SubscriptionsComponent},
    { path:'channels',component:ChannelsComponent},
    { path:'connections',component:ConnectionsComponent},
    { path:'setting',component:SettingComponent},
    { path:'playing/:id',component:VideoPlayerComponent},
    
    { 
      path:'profile',
      component:ProfileComponent,
      children:[
        { path:'',redirectTo:'about-me',pathMatch:'full' },
        { path:'about-me',component:AboutUserComponent},
        { path:'my-videos',component:UsersVideosComponent},
        { path:'my-channels',component:UsersChannelsComponent}
      ]
    },
  ]
  }

]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class VideoAppRoutingModule{ }
