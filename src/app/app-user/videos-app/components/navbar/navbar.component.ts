import { ElementRef, ViewChild, Component, OnInit, HostListener} from '@angular/core';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'video-app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [
    trigger('animateSideBar', [
      state('in', style({
        marginLeft: '-200px'
      })),
      state('out', style({
        marginLeft: '-10px'
      })),
      transition('in => out', animate('200ms ease-in-out')),
      transition('out => in', animate('200ms ease-in-out'))
    ])
  ]
})
export class NavbarComponent implements OnInit {
  @ViewChild('sidebar') sidebar:ElementRef;
  menuState:string = 'out';
  
  constructor(private router:Router) { 
    
  }
  
  ngOnInit() {
    this.menuState = 'in';
  }
  
  
  toggleSideBar() {
    if(this.menuState == 'in'){
      this.menuState = 'out';
    }
    else{
      this.menuState = 'in';
    }
    this.sidebar.nativeElement.style.display = "block";
  }
  
  @HostListener('window:keydown', ['$event', '$event.target'])
  keyboardInput(event: KeyboardEvent) {
    if(event.keyCode == 27){
      this.menuState = 'in';
    }
  }
  
  @HostListener('window:click', ['$event', '$event.target'])
  mouseEvent(event) {
    let targets = ["menuIcon"];
    if(event.target.parentElement === null){
      this.menuState = 'in';
    }
    else{
      let target = event.target.parentElement.getAttribute('id');
      if(target == null || (target != null && targets.indexOf(target)!=0)){
        this.menuState = 'in';
      }
    }
  }
  
}
