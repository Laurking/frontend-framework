import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute,NavigationStart,NavigationEnd,RoutesRecognized,Router} from '@angular/router';
import { Video } from '../../entities/video';
import { VideoService } from '../../services/video-service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit, OnDestroy {
  private currentVideo:Video = new Video();
  private stub:any;
  private vid:any;
  

  constructor(
    private route:ActivatedRoute,
    private videoService:VideoService
  ) { }

  ngOnInit() {
    this.stub = this.route.params.subscribe(params => {
      const id = +params['id']; 
      this.vid = this.videoService.getVideo(id).subscribe(data =>{
        this.currentVideo = data;
        console.log(this.currentVideo);
      },error=>{
        console.log(error);
      })
    });
  }

  ngOnDestroy(){
    this.stub.unsubscribe();
    this.vid.unsubscribe();
  }

}
