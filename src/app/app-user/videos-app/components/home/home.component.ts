import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationStart,NavigationEnd,RoutesRecognized,Router} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser'
import { Video } from '../../entities/video';
import { VideoService } from '../../services/video-service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy{
 
  private array:any[] = new Array();
  private recentVideos:Video[] = new Array();
  private suggestedVideos:Video[] = new Array();
  private videos:Video[] = new Array();
  private stub:any;
  private loadingRecentVideos:boolean;
  private loadingSuggestedVideos:boolean;
 
  constructor(
    private router:Router,
    private domSanitizer:DomSanitizer,
     private videoService:VideoService
    ) { }

  ngOnInit() {
    this.loadingSuggestedVideos = true;
    this.videoService.getVideos().subscribe((data)=>{
      this.suggestedVideos = data;
      this.loadingSuggestedVideos = false;
    },error=>{
      console.log(error)
    })

    this.loadingRecentVideos = true;
    this.videoService.getVideos().subscribe((data)=>{
      this.recentVideos = data;
      this.loadingRecentVideos = false;
    },error=>{
      console.log(error)
    })
  }

  ngOnDestroy() {
   
  }

}
