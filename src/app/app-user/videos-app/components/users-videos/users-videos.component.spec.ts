import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersVideosComponent } from './users-videos.component';

describe('UsersVideosComponent', () => {
  let component: UsersVideosComponent;
  let fixture: ComponentFixture<UsersVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
