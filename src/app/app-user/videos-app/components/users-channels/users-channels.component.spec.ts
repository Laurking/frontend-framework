import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersChannelsComponent } from './users-channels.component';

describe('UsersChannelsComponent', () => {
  let component: UsersChannelsComponent;
  let fixture: ComponentFixture<UsersChannelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersChannelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersChannelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
