import { Component, OnInit ,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { Video } from '../../entities/video';
import { VideoService } from '../../services/video-service';
import { FileData } from '../../entities/transfer-file-data';


@Component({
  selector: 'app-upload-video',
  templateUrl: './upload-video.component.html',
  styleUrls: ['./upload-video.component.scss'],
  providers:[VideoService]
  
})
export class UploadVideoComponent implements OnInit {
  
  @ViewChild('inputFiles') inputFiles;
  @ViewChild("progressBar") progressBar;
  private videoFilesToUpload:FileData[] = new Array();
  private files:File[] = new Array();
  private uploadStatus:string;
  private fileUPloadStatus:string;
  
  
  progress: { percentage: number } = { percentage: 0 }
  
  constructor(private videoService:VideoService, private router:Router) {
    
   }
  
  ngOnInit() {
    this.uploadStatus = "upload";
    this.fileUPloadStatus="upload";

    
  }
  
  changeUploadStatus(){
    this.uploadStatus = "upload";
    this.fileUPloadStatus="upload";
    this.router.navigate(['/marialex/video/upload']);
    
  }

  handleFiles(){
    this.inputFiles.nativeElement.parentNode.style.background="#CCCCCC";
  }
  
  onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.inputFiles.nativeElement.parentNode.style.background="#CCCCCC";
    
  }
  
  onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.inputFiles.nativeElement.parentNode.style.background="#FFFFFF";
    
  }
  
  
  onDrop(event: any) {
    event.preventDefault();
    event.stopPropagation();
    let dropBoxFiles:FileList = event.dataTransfer.files;
    Array.from(dropBoxFiles).forEach(file => {
      if(file!=null){
        let fileData = new FileData(file.name, file.size,file.type);
        this.files.push(file);
        this.videoFilesToUpload.push(fileData);
      }
    });
    
  }
  
  uploadFiles(){
    let inputData:FileList = this.inputFiles.nativeElement.files;
    Array.from(inputData).forEach(file => {
      if(file!=null){
        let fileData = new FileData(file.name, file.size,file.type);
        this.files.push(file);
        this.videoFilesToUpload.push(fileData); 
      }
    });
    
    if(this.files != null){
      this.videoService.addVideos(this.files).subscribe(event=>{
        if (event.type === HttpEventType.UploadProgress) {
          this.progress.percentage = Math.round(100 * event.loaded / event.total);
          this.uploadStatus="uploading";
          this.fileUPloadStatus="uploading";
        } else if (event instanceof HttpResponse) {
          this.fileUPloadStatus="completed";
        }
      },error=>{
        console.log(error);
      })
    }
    
  }
  
}
