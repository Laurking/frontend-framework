export class FileData{
    name:string;
    size: number;
    type:string;
    file:File;
    
    constructor(name:string,size:number,type:string){
        this.name = name;
        this.size = size;
        this.type = type;
    }

    setName(name:string){
        this.name = name;
    }
    getName(){
      return this.name;
    }

    setSize(size:number){
        this.size = size;
    }
    getSize(){
      return this.size;
    }

    setType(type:string){
        this.type = type;
    }
    getType(){
      return this.type;
    }
}