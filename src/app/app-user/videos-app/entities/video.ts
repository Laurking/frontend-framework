export class Video {
    id:number;
    filename:string;
    source:number[];
    type:string;
    size: number;
    userId:number;
    createdOn:string;
    updatedOn:string;
}

