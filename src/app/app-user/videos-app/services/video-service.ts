import { Injectable } from '@angular/core';
import {Http,HttpModule, Headers, RequestOptions,Response } from '@angular/http';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import { Urls } from '../../../shared-features/constants/urls';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { Video } from '../entities/video';


@Injectable()
export class VideoService {
  
  private baseUrl:string = Urls.baseUrl;
  private headers = new Headers({'content-type':'application/json'});
  private options = new RequestOptions({headers:this.headers});
  
  constructor(private httpService:HttpClient,private http: HttpClient) { }

 
  addVideos(files:File[]): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    for(let file of files){
      formData.append('videoFiles',file);
    }
    const req = new HttpRequest('POST', this.baseUrl+"/video/add-video", formData, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }
 
  getVideos(): Observable<Video[]>{
    return this.httpService.get(this.baseUrl+"/video/all-videos")
    .map((response:Response)=>response)
    .catch(this.errorHandler);
  }

  getVideo(videoId:number): Observable<Video>{
    return this.httpService.get(this.baseUrl+"/videos/find-video/"+videoId)
    .map((response:Response) => response)
    .catch(this.errorHandler);
  }
  
  errorHandler(error:Response){
    return Observable.throw(error || 'SERVER ERROR');
  }

  
  
}
