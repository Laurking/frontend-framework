import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SharedModuleModule } from '../../shared-features/modules/shared-module.module';
import { VideoAppComponent } from './video-app.component';
import { VideoAppRoutingModule } from './video-app.routing';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { UploadVideoComponent } from './components/upload-video/upload-video.component';
import { SearchComponent } from './components/search/search.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { ChannelsComponent } from './components/channels/channels.component';
import { ConnectionsComponent } from './components/connections/connections.component';
import { AboutUserComponent } from './components/about-user/about-user.component';
import { UsersVideosComponent } from './components/users-videos/users-videos.component';
import { UsersChannelsComponent } from './components/users-channels/users-channels.component';
import { SettingComponent } from './components/setting/setting.component';
import { VideoPlayerComponent } from './components/video-player/video-player.component';
import { VideoService } from './services/video-service';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { UserService } from '../../shared-features/services/user-service';
import { AuthGuard } from '../../shared-features/auth/auth.guard';










@NgModule({
  declarations: [
    VideoAppComponent,
    HomeComponent,
    NavbarComponent,
    NavbarComponent,
    UploadVideoComponent,
    SearchComponent,
    NotificationsComponent,
    ProfileComponent,
    SubscriptionsComponent,
    ChannelsComponent,
    ConnectionsComponent,
    AboutUserComponent,
    UsersVideosComponent,
    UsersChannelsComponent,
    SettingComponent,
    VideoPlayerComponent,
    SidebarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    VideoAppRoutingModule,
    SharedModuleModule
  ],
  providers: [AuthGuard, UserService, VideoService],
  bootstrap: [VideoAppComponent]
})
export class VideoAppModule { }
