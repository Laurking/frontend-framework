import { Component, OnInit } from '@angular/core';
import { AuthenticationDetails } from '../../../../shared-features/users/entities/authentication-details';
import { LoginCredentials } from '../../../../shared-features/users/entities/login-credentials';
import { Router } from '@angular/router';
import { UserService } from '../../../../shared-features/services/user-service';
import { FormValidation } from '../../../../shared-features/models/form-validation.model';
import { User } from '../../../../shared-features/users/entities/user';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private authenticationFailure:boolean = false;
  private formValidation:FormValidation = new FormValidation();
  constructor(private userService:UserService,
    private router:Router) { 
      this.formValidation.createLoginFormValidation()
    }
    
    ngOnInit() {
      
    }

    emailFocus(){
      this.authenticationFailure = false;
    }
    
    passwordFocus(){
      this.authenticationFailure = false;
    }
    login(loginCredentials){
      this.userService.authenticateUser(loginCredentials).subscribe(data =>{
        let authenticationDetails:AuthenticationDetails = new AuthenticationDetails();
        authenticationDetails = data;
        localStorage.setItem("token",authenticationDetails.token);
        this.router.navigate(['/marialex'])
      },error =>{
        console.log(error);
        this.authenticationFailure = true;
      })
    }
  }
  