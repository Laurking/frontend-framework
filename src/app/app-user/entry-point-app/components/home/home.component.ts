import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../shared-features/services/user-service';
import { User } from '../../../../shared-features/users/entities/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  constructor(private userService:UserService) { }

  ngOnInit() {
    this.userService.getUserDetails(localStorage.getItem("token")).subscribe(data =>{
      let user:User =  data;
      sessionStorage.setItem('currentUser',JSON.stringify(user));
    },error =>{
      console.log(error);
    })
  }

}
