import { ElementRef, ViewChild, Component, OnInit, OnDestroy } from '@angular/core';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';
import { AuthenticationDetails } from '../../../../shared-features/users/entities/authentication-details';

import { ActivatedRoute,NavigationStart,NavigationEnd,RoutesRecognized,Router} from '@angular/router';
import {Location} from '@angular/common';
import { UserService } from '../../../../shared-features/services/user-service';


@Component({
  selector: 'entry-app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [
		trigger(
			'enterAnimation', [
				transition(':enter', [
					style({transform: 'translateX(100%)', opacity: 0}),
					animate('400ms', style({transform: 'translateX(0)', opacity: 1}))
				]),
				transition(':leave', [
					style({transform: 'translateX(0)', opacity: 1}),
					animate('400ms', style({transform: 'translateX(100%)', opacity: 0}))
				])
			]
		)
		
	]
})
export class NavbarComponent implements OnInit, OnDestroy {
  @ViewChild('navigationBar') navigationBar:ElementRef;
  private isLoggedIn:boolean = false;
  private logoutResponse:any;
  private logoutFromAllRout:any;
  private stub:any;
  constructor(private userService:UserService,
    private route: ActivatedRoute,
    private location: Location,
    private router:Router) {  
    }
    
    ngOnInit() {
      if(localStorage.getItem('token') != null){
        this.isLoggedIn = true;
      }
      this.stub = this.router.events.subscribe(event =>{
        if(event instanceof NavigationStart){
          const url = this.router.url;
          this.location.replaceState(url);
          if(localStorage.getItem('token') != null){
            this.isLoggedIn = true;
          }
        }
        if(event instanceof NavigationEnd){
          const url = this.router.url;
          this.location.replaceState(url);
          if(localStorage.getItem('token') != null){
            this.isLoggedIn = true;
          }
        }
      })
      let width =  window.innerWidth;
      if(width>=500){
        this.navigationBar.nativeElement.style.display = "block";
      }
      else{
        this.navigationBar.nativeElement.style.display = "none";
      }
    }
    
    ngOnDestroy(){
      this.stub.unsubscribe();
    }
    
    private onResize(event) {
      let width = event.target.innerWidth;
      if(width>=500){
        this.navigationBar.nativeElement.style.display = "block";
      }
      else{
        this.navigationBar.nativeElement.style.display = "none";
      }
    }
    
    toggleMenu = function(){
      let navigationStatus = this.navigationBar.nativeElement.style.display;
      if(navigationStatus == "" || navigationStatus == null || navigationStatus == "none"){
        this.navigationBar.nativeElement.style.display = "block";
      }
      else if(navigationStatus == "block"){
        this.navigationBar.nativeElement.style.display = "none";
      }
      
    }
    
    toggleAndRedirect(){
      let width =  window.innerWidth;
      if(width<500){
        let navigationStatus = this.navigationBar.nativeElement.style.display;
        if(navigationStatus == "" || navigationStatus == null || navigationStatus == "none"){
          this.navigationBar.nativeElement.style.display = "block";
        }
        else if(navigationStatus == "block"){
          this.navigationBar.nativeElement.style.display = "none";
        }
      }
      
    }
    
    logout(){
      this.logoutFromAllRout= this.userService.logout().subscribe(data =>{
        this.logoutResponse = data;
        this.isLoggedIn = false;
        localStorage.removeItem('token')
        this.router.navigate(['/marialex/login'])
      },error =>{
        console.log(error);
      })
    }
    
  }
  