import { Component, OnInit } from '@angular/core';
import { User } from '../../../../shared-features/users/entities/user';
import { UserService } from '../../../../shared-features/services/user-service';
import { FormValidation } from '../../../../shared-features/models/form-validation.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
 private user:User = new User();
 private subscriber:any;
 private formValidation:FormValidation = new FormValidation();
  constructor(private router:Router,private userService:UserService) { 
    this.formValidation.createRegistrationFormValidation();
    
  }

  ngOnInit() {
  }

  register(registrationData){
    this.subscriber = this.userService.registration(registrationData).subscribe(data=>{
      this.user = data;
      this.router.navigate(['/marialex/login'])
    },error =>{
      console.log(error);
    })
  }
}
