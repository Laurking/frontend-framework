import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EntryPointRoutingModule } from './entry-point-routing.module';
import { SharedModuleModule } from '../../shared-features/modules/shared-module.module';
import { UserService } from '../../shared-features/services/user-service';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { OauthComponent } from './components/oauth/oauth.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { EntryPointComponent } from './entry-point.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SearchComponent } from './components/search/search.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './components/terms-of-service/terms-of-service.component';
import { RetrievePasswordComponent } from './components/retrieve-password/retrieve-password.component';
import { UnauthorizedAccessGuard } from '../../shared-features/auth/unauthorized-access.guard';








@NgModule({
  declarations: [
    EntryPointComponent,
    HomeComponent,
    AboutComponent,
    OauthComponent,
    RegistrationComponent,
    LoginComponent,
    NavbarComponent,
    SearchComponent,
    PrivacyPolicyComponent,
    TermsOfServiceComponent,
    RetrievePasswordComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    EntryPointRoutingModule,
    SharedModuleModule
  ],
  providers: [UnauthorizedAccessGuard,UserService],
  bootstrap: [EntryPointComponent]
})
export class EntryPointAppModule { }
