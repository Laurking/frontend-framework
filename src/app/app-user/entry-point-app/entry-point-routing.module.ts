import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { OauthComponent } from './components/oauth/oauth.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { EntryPointComponent } from './entry-point.component';
import { SearchComponent } from './components/search/search.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './components/terms-of-service/terms-of-service.component';
import { RetrievePasswordComponent } from './components/retrieve-password/retrieve-password.component';
import { UnauthorizedAccessGuard } from '../../shared-features/auth/unauthorized-access.guard';


const routes: Routes = [
  
  { path: 'marialex', 
  component:EntryPointComponent,
  children:[
    { path: '', component:HomeComponent},
    { path: 'search', component:SearchComponent},
    { path: 'about', component:AboutComponent},
    { path: 'privacy-policy', component:PrivacyPolicyComponent},
    { path: 'terms-of-service', component:TermsOfServiceComponent},
    { path: 'retrieve-password', component:RetrievePasswordComponent},
    { path: 'register', component:OauthComponent,
    canActivate:[UnauthorizedAccessGuard],
    children:[ { path:'', component:RegistrationComponent}]
  },
  { path: 'login', component:OauthComponent,
  canActivate:[UnauthorizedAccessGuard],
  children:[ { path:'', component:LoginComponent}]
}
]
}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
})
export class EntryPointRoutingModule{ }
