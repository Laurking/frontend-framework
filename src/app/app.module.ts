import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';

import { SharedModuleModule } from './shared-features/modules/shared-module.module';
import { EntryPointAppModule } from './app-user/entry-point-app/entry.module';
import { VideoAppModule } from './app-user/videos-app/video-app.module';
import { FeedAppModule } from './app-user/feeds-app/feed-root.module';
import { AppRoutingModule } from './app.routing';






@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModuleModule,
    AppRoutingModule,
    EntryPointAppModule,
    VideoAppModule,
    FeedAppModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
